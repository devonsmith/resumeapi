namespace ResumeAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Project")]
    public partial class Project
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Project()
        {
            ProjectSkills = new HashSet<ProjectSkill>();
        }

        public int ID { get; set; }

        public int? UserID { get; set; }

        [Required]
        [StringLength(1024)]
        public string Name { get; set; }

        [Required]
        [StringLength(1024)]
        public string ProjectFocus { get; set; }

        [Required]
        [StringLength(255)]
        public string ProjectLink { get; set; }

        [Required]
        [StringLength(2048)]
        public string Description { get; set; }

        public virtual Profile Profile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProjectSkill> ProjectSkills { get; set; }
    }
}
