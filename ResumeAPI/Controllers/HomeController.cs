﻿using ResumeAPI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ResumeAPI.Controllers
{
    public class HomeController : Controller
    {
        ResumeDataContext db = new ResumeDataContext();
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View(db.Profiles.ToList());
        }
    }
}
